# PLACZEK CODE

 INSTALLATION

 In order to compile the source code, edit "compile.sh" file (inside the folder "Placzek") replacing "gfortran" with the fortran compiler of your system.

 Then, simply type "source compile.sh" to create the "placzek.x" executable.

 ------------------------------------------------------------------------------------------

 LEARNING AND USING PLACZEK

 Notice there are four other directories therein:

 1. qtaim_ready/
 2. qtaim_sample/
 3. hirsh_ready/
 4. hirsh_sample/

 The folders marked as "_ready"  contain complete AC/DC analyses using QTAIM or Hirshfeld atomic charges and atomic dipoles, respectively. The folders marked as "_sample" contain the inputs which will reproduce these analyses assuming Placzek will run appropriately. After compiling Placzek, please use the "_sample" folders and inputs therein to test if Placzek runs without errors. Thereafter, compare the "PLACZEK.OUT" file with the ones in "_ready". Minor differences can be accounted for numerical errors, but these differences are usually very small or even null.

 In order to perform the AC/DC analysis of IR intensities and its subsequent CCTDP partition of them, Placzek needs three main inputs:
 1. PLACZEK.INP
 2. HESSIAN
 3. GAUSSIAN.com


 1. PLACZEK.INP  contains the informations that will control the internal Placzek setup. We'll not explain all the keywords, but only the ones which the $USER might want to modify. For instance, the paths for the "Friends" (the external programs Placzek will eventually call) and the displacement (we recommend 0.01 Angs, but any value is acceptable). It is worth to mention that the structure is a bit different for the AC/DC analysis using QTAIM or Hirshfeld charges and dipoles; inputs for each case are made available in the folders "_sample".

 2. HESSIAN contains the Force Constants (the second derivatives of energy) necessary for the normal mode analysis and evaluation of vibrational frequencies (notice Placzek calculate the frequencies and intensities, not just copy them! The frequencies must agree with the ones from Gaussian, and the intensities will agree if the molecular dipole moment is correctly reproduced). Since HESSIAN only requires the force constants, it doesn't matter if the complete output from the freq=noraman job of Gaussian is used or only the section headed by "Force constants in Cartesian coordinates", which Gaussian prints under the the keyword "iop(7/33=1)" ; see the h2o-fv.com and h2o-fv.log files for details.

 3. GAUSSIAN.com contains the input for electronic structure calculations using Gaussian. It is composed by a regular file for a Single Point calculation, but observing the "output=wfn/wfx" keyword (if QTAIM charges and dipoles are wanted) or "pop=hirshfeld" (if Hirshfeld parameters are wanted). Notice that if QTAIM is chosen, "GAUSSIAN.wfn/wfx" must be inserted at the end; this file name is not optional. Additional keywords can be used, though they're not required (scf=tight, grid=ultrafine, other $pop options etc.) Since these are calculations related to vibrational analysis, it is important to use optimized structures in the GAUSSIAN.com file, namely the structure that provided the vibrational analysis which delivered the HESSIAN file.
 ------------------------------------------------------------------------------------------

 TRICK FOR ANY-CHARGE-MODEL ANALYSIS
 
 While Placzek can perform this analysis for QTAIM, Hirshfeld and ChelpG models, there are many other available in the literature. If you want to perform an AC/DC analysis using one of these other models, please contact us and we will manage to help you:
 Main developer: LN Vidal,   lnvidal@utfpr.edu.br
 Usage support:  WE Richter, richter@utfpr.edu.br 
